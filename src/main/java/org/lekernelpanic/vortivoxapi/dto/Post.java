package org.lekernelpanic.vortivoxapi.dto;

public class Post {

    private String id;
    private String username;
    private String content;
    private int score;

    public String getId() {
        return id;
    }

    public Post setId(String id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Post setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Post setContent(String content) {
        this.content = content;
        return this;
    }

    public int getScore() {
        return score;
    }

    public Post setScore(int score) {
        this.score = score;
        return this;
    }

}

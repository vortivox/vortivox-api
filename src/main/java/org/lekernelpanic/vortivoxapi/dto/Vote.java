package org.lekernelpanic.vortivoxapi.dto;

public class Vote {

    private Post post;
    private boolean up;

    public Post getPost() {
        return post;
    }

    public Vote setPost(Post post) {
        this.post = post;
        return this;
    }

    public boolean isUp() {
        return up;
    }

    public Vote setUp(boolean up) {
        this.up = up;
        return this;
    }

}

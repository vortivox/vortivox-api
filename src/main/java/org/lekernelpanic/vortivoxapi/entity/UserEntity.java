package org.lekernelpanic.vortivoxapi.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    private String name;
    private String password;
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<PostEntity> posts;
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<TokenEntity> tokens;
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<VoteEntity> votes;

    public String getName() {
        return name;
    }

    public UserEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserEntity setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<PostEntity> getPosts() {
        return posts;
    }

    public UserEntity setPosts(List<PostEntity> posts) {
        this.posts = posts;
        return this;
    }

    public List<TokenEntity> getTokens() {
        return tokens;
    }

    public UserEntity setTokens(List<TokenEntity> tokens) {
        this.tokens = tokens;
        return this;
    }

    public List<VoteEntity> getVotes() {
        return votes;
    }

    public UserEntity setVotes(List<VoteEntity> votes) {
        this.votes = votes;
        return this;
    }

}

package org.lekernelpanic.vortivoxapi.entity;

import jakarta.persistence.*;
import org.lekernelpanic.vortivoxapi.entity.id.VoteId;

@Entity
@Table(name = "vote")
@IdClass(VoteId.class)
public class VoteEntity {

    @Id
    @ManyToOne
    @JoinColumn(name = "post")
    private PostEntity post;
    @Id
    @ManyToOne
    @JoinColumn(name = "user")
    private UserEntity user;
    private boolean up;

    public PostEntity getPost() {
        return post;
    }

    public VoteEntity setPost(PostEntity post) {
        this.post = post;
        return this;
    }

    public UserEntity getUser() {
        return user;
    }

    public VoteEntity setUser(UserEntity user) {
        this.user = user;
        return this;
    }

    public boolean isUp() {
        return up;
    }

    public VoteEntity setUp(boolean up) {
        this.up = up;
        return this;
    }

}

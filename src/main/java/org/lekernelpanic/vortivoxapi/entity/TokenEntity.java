package org.lekernelpanic.vortivoxapi.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "token")
public class TokenEntity {

    @Id
    @GeneratedValue
    private UUID token;
    @ManyToOne
    @JoinColumn(name = "user")
    private UserEntity user;
    private LocalDateTime timestamp;

    public UUID getToken() {
        return token;
    }

    public TokenEntity setToken(UUID token) {
        this.token = token;
        return this;
    }

    public UserEntity getUser() {
        return user;
    }

    public TokenEntity setUser(UserEntity user) {
        this.user = user;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public TokenEntity setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

}

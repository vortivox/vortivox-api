package org.lekernelpanic.vortivoxapi.entity;

import jakarta.persistence.*;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "post")
public class PostEntity {

    @Id
    @GeneratedValue
    private UUID id;
    private String content;
    @ManyToOne
    @JoinColumn(name = "user")
    private UserEntity user;
    @OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
    private List<VoteEntity> votes;

    public UUID getId() {
        return id;
    }

    public PostEntity setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return content;
    }

    public PostEntity setContent(String content) {
        this.content = content;
        return this;
    }

    public UserEntity getUser() {
        return user;
    }

    public PostEntity setUser(UserEntity user) {
        this.user = user;
        return this;
    }

    public List<VoteEntity> getVotes() {
        return votes;
    }

    public PostEntity setVotes(List<VoteEntity> votes) {
        this.votes = votes;
        return this;
    }

}
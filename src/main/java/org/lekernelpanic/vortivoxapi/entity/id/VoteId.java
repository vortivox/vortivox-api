package org.lekernelpanic.vortivoxapi.entity.id;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class VoteId implements Serializable {

    private UUID post;
    private String user;

    public UUID getPost() {
        return post;
    }

    public VoteId setPost(UUID post) {
        this.post = post;
        return this;
    }

    public String getUser() {
        return user;
    }

    public VoteId setUser(String user) {
        this.user = user;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteId voteId = (VoteId) o;
        return Objects.equals(post, voteId.post) && Objects.equals(user, voteId.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(post, user);
    }

}

package org.lekernelpanic.vortivoxapi.job;

import org.lekernelpanic.vortivoxapi.service.UserService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TokenJob {

    private final UserService userService;

    public TokenJob(UserService userService) {
        this.userService = userService;
    }


    @Scheduled(fixedDelayString = "${token.job-delay}")
    public void removeExpiredTokens() {
        userService.deleteExpiredTokens();
    }

}

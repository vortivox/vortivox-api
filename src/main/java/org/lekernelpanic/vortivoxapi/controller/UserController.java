package org.lekernelpanic.vortivoxapi.controller;

import org.lekernelpanic.vortivoxapi.dto.User;
import org.lekernelpanic.vortivoxapi.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/users")
public class UserController {

    private final int usernameMaxLength;
    private final int passwordMaxLength;
    private final UserService userService;

    public UserController(
            @Value("${user.username-max-length}") int usernameMaxLength,
            @Value("${user.password-max-length}") int passwordMaxLength,
            UserService userService
    ) {
        this.usernameMaxLength = usernameMaxLength;
        this.passwordMaxLength = passwordMaxLength;
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody User user) {
        if (user.getName().length() > usernameMaxLength || user.getPassword().length() > passwordMaxLength
                || user.getName().isEmpty() || user.getPassword().isEmpty())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        return ResponseEntity.status(userService.save(user) ? HttpStatus.CREATED : HttpStatus.UNAUTHORIZED).build();
    }

    @DeleteMapping
    public ResponseEntity<Void> delete(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return userService.findUsernameFrom(authorizationHeader).map(username -> {
            userService.delete(username);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }).orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @GetMapping("/credentials")
    public ResponseEntity<String> whoAmI(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return userService.findUsernameFrom(authorizationHeader)
                .map(username -> ResponseEntity.status(HttpStatus.OK).body(username))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PostMapping("/credentials")
    public ResponseEntity<String> logIn(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return userService.logIn(authorizationHeader)
                .map(token -> ResponseEntity.status(HttpStatus.OK).body(token))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @DeleteMapping("/credentials")
    public ResponseEntity<Void> logOut(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        userService.logOut(authorizationHeader);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}

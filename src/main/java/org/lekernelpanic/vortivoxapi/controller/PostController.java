package org.lekernelpanic.vortivoxapi.controller;

import org.lekernelpanic.vortivoxapi.dto.Post;
import org.lekernelpanic.vortivoxapi.dto.Vote;
import org.lekernelpanic.vortivoxapi.service.PostService;
import org.lekernelpanic.vortivoxapi.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/posts")
public class PostController {

    private final int postMaxLength;
    private final PostService postService;
    private final UserService userService;

    public PostController(
            @Value("${post.max-length}") int postMaxLength,
            PostService postService, UserService userService
    ) {
        this.postMaxLength = postMaxLength;
        this.postService = postService;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<Post>> getAll(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return userService.findUsernameFrom(authorizationHeader)
                .map(username -> ResponseEntity.status(HttpStatus.OK).body(postService.getPosts(username)))
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @GetMapping("/random")
    public ResponseEntity<Post> getRandom(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        return userService.findUsernameFrom(authorizationHeader)
                .map(username -> ResponseEntity.of(postService.randomPost()))
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping
    public ResponseEntity<Void> post(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader,
            @RequestBody Post post
    ) {
        return userService.findUsernameFrom(authorizationHeader).map(username -> {
            if (post.getContent().length() > postMaxLength) return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
            postService.post(post.setUsername(username));
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }).orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader,
            @PathVariable UUID id
    ) {
        return userService.findUsernameFrom(authorizationHeader).map(username -> {
            postService.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }).orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @GetMapping("/votes")
    public ResponseEntity<List<Vote>> getAllVotes(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader
    ) {
        return userService.findUsernameFrom(authorizationHeader)
                .map(postService::getVotes)
                .map(votes -> ResponseEntity.status(HttpStatus.OK).body(votes))
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping("/{id}/votes")
    public ResponseEntity<Void> vote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader,
            @PathVariable UUID id,
            @RequestParam boolean up
    ) {
        return userService.findUsernameFrom(authorizationHeader).map(username -> {
            boolean saved = postService.vote(id, username, up);
            return new ResponseEntity<Void>(saved ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        }).orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @DeleteMapping("/{id}/votes")
    public ResponseEntity<Void> deleteVote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader,
            @PathVariable UUID id
    ) {
        return userService.findUsernameFrom(authorizationHeader).map(username -> {
            postService.deleteVote(id, username);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }).orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

}

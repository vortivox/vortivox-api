package org.lekernelpanic.vortivoxapi.repository;

import org.lekernelpanic.vortivoxapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
}

package org.lekernelpanic.vortivoxapi.repository;

import org.lekernelpanic.vortivoxapi.entity.PostEntity;
import org.lekernelpanic.vortivoxapi.entity.VoteEntity;
import org.lekernelpanic.vortivoxapi.entity.id.VoteId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoteRepository extends JpaRepository<VoteEntity, VoteId> {

    List<VoteEntity> findByUserName(String username);

}

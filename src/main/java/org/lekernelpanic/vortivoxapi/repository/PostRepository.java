package org.lekernelpanic.vortivoxapi.repository;

import org.lekernelpanic.vortivoxapi.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, UUID> {

    List<PostEntity> findByUserName(String username);

}

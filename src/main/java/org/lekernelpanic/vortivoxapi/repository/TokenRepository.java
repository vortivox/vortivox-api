package org.lekernelpanic.vortivoxapi.repository;

import org.lekernelpanic.vortivoxapi.entity.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface TokenRepository extends JpaRepository<TokenEntity, UUID> {

    @Modifying
    @Transactional
    @Query("DELETE FROM TokenEntity t WHERE t.timestamp < :cutoffTimestamp")
    void deleteTokensOlderThan(@Param("cutoffTimestamp") LocalDateTime cutoffTimestamp);

}

package org.lekernelpanic.vortivoxapi.service;

import org.apache.commons.lang3.tuple.Pair;
import org.lekernelpanic.vortivoxapi.dto.Post;
import org.lekernelpanic.vortivoxapi.dto.Vote;
import org.lekernelpanic.vortivoxapi.entity.PostEntity;
import org.lekernelpanic.vortivoxapi.entity.UserEntity;
import org.lekernelpanic.vortivoxapi.entity.VoteEntity;
import org.lekernelpanic.vortivoxapi.entity.id.VoteId;
import org.lekernelpanic.vortivoxapi.repository.PostRepository;
import org.lekernelpanic.vortivoxapi.repository.VoteRepository;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Service
public class PostService {

    private static final Random RANDOM = new Random();
    private static final double SIGMOID_SCALING_FACTOR = 0.1;
    private final PostRepository postRepository;
    private final VoteRepository voteRepository;

    public PostService(PostRepository postRepository, VoteRepository voteRepository) {
        this.postRepository = postRepository;
        this.voteRepository = voteRepository;
    }

    /**
     * @param username of a user
     * @return all posts from a user
     */
    public List<Post> getPosts(String username) {
        return postRepository.findByUserName(username).stream().map(this::map).toList();
    }

    /**
     * @return a post randomly picked pondered with votes
     */
    public Optional<Post> randomPost() {
        List<Pair<PostEntity, Double>> posts = postRepository.findAll()
                .stream()
                .map(postEntity -> Pair.of(postEntity, sigmoid(postEntity.getVotes().size())))
                .toList();

        if (posts.isEmpty())
            return Optional.empty();

        double totalWeight = posts.stream().mapToDouble(Pair::getRight).sum();
        double randomNumber = RANDOM.nextDouble(totalWeight);

        double partialSum = 0;
        for (Pair<PostEntity, Double> post : posts) {
            partialSum += post.getRight();
            if (randomNumber < partialSum) {
                return Optional.of(map(post.getLeft()));
            }
        }

        return Optional.empty();
    }

    /**
     * @param post save the post
     */
    public void post(Post post) {
        postRepository.save(map(post));
    }

    /**
     * @param id delete the post corresponding this id
     */
    public void delete(UUID id) {
        postRepository.deleteById(id);
    }

    /**
     * @param username the corresponding username
     * @return all the votes corresponding this username
     */
    public List<Vote> getVotes(String username) {
        return voteRepository.findByUserName(username).stream().
                map(voteEntity -> new Vote().setPost(map(voteEntity.getPost())).setUp(voteEntity.isUp()))
                .toList();
    }

    /**
     * @param id       vote for the corresponding id
     * @param username vote for the corresponding username
     * @param up       is the vote positive or negative
     * @return true if the vote is saved, false if id or username not found
     */
    public boolean vote(UUID id, String username, boolean up) {
        try {
            voteRepository.save(
                    new VoteEntity()
                            .setPost(new PostEntity().setId(id))
                            .setUser(new UserEntity().setName(username))
                            .setUp(up)
            );
            return true;
        } catch (JpaObjectRetrievalFailureException e) {
            return false;
        }
    }

    /**
     * @param id       deletes vote corresponding this id
     * @param username deletes vote corresponding this username
     */
    public void deleteVote(UUID id, String username) {
        voteRepository.deleteById(new VoteId()
                .setPost(id)
                .setUser(username));
    }

    /**
     * @param postEntity entity to map
     * @return mapped dto
     */
    private Post map(PostEntity postEntity) {
        int score = postEntity.getVotes().stream()
                .map(voteEntity -> voteEntity.isUp() ? 1 : -1)
                .mapToInt(Integer::intValue)
                .sum();
        return new Post()
                .setId(postEntity.getId().toString())
                .setUsername(postEntity.getUser().getName())
                .setContent(postEntity.getContent())
                .setScore(score);
    }

    /**
     * @param post dto to map
     * @return entity mapped
     */
    private PostEntity map(Post post) {
        return new PostEntity()
                .setUser(new UserEntity().setName(post.getUsername()))
                .setContent(post.getContent());
    }

    /**
     * @param x the input value
     * @return value mapped between 0  and 1
     */
    private double sigmoid(int x) {
        return 1.0 / (1.0 + Math.exp(-SIGMOID_SCALING_FACTOR * x));
    }

}

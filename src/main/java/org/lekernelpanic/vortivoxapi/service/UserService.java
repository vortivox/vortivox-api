package org.lekernelpanic.vortivoxapi.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.lekernelpanic.vortivoxapi.dto.User;
import org.lekernelpanic.vortivoxapi.entity.TokenEntity;
import org.lekernelpanic.vortivoxapi.entity.UserEntity;
import org.lekernelpanic.vortivoxapi.repository.TokenRepository;
import org.lekernelpanic.vortivoxapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final long validityMaxTime;
    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;

    public UserService(
            @Value("${token.validity-max-time}") long validityMaxTime,
            UserRepository userRepository,
            TokenRepository tokenRepository
    ) {
        this.validityMaxTime = validityMaxTime;
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
    }

    /**
     * @param authorizationHeader http authorization header
     * @return the connection token, empty if connection failed
     */
    public Optional<String> logIn(String authorizationHeader) {
        return getUserFromHeader(authorizationHeader)
                .flatMap(user -> isUserValid(user) ? Optional.of(generateToken(user)) : Optional.empty());
    }

    /**
     * @param authorizationHeader http authorization header to disconnect
     */
    public void logOut(String authorizationHeader) {
        getTokenFromHeader(authorizationHeader)
                .ifPresent(tokenRepository::deleteById);
    }

    /**
     * @param authorizationHeader http authorization header
     * @return username from http authorization header
     */
    public Optional<String> findUsernameFrom(String authorizationHeader) {
        return getTokenFromHeader(authorizationHeader)
                .flatMap(tokenRepository::findById)
                .map(TokenEntity::getUser)
                .map(UserEntity::getName);
    }

    /**
     * @param user to save
     * @return true if the user is saved, false if already exists
     */
    public boolean save(User user) {
        if (userRepository.existsById(user.getName()))
            return false;

        UserEntity userEntity = new UserEntity()
                .setName(user.getName())
                .setPassword(DigestUtils.sha256Hex(user.getPassword()));
        userRepository.save(userEntity);

        return true;
    }

    /**
     * @param username of the user to delete
     */
    public void delete(String username) {
        userRepository.deleteById(username);
    }

    /**
     * Delete all expired tokens.
     */
    public void deleteExpiredTokens() {
        tokenRepository.deleteTokensOlderThan(LocalDateTime.now().minusSeconds(validityMaxTime));
    }

    /**
     * @param authorizationHeader http authorization header
     * @return a created token for the authorization header
     */
    private Optional<UUID> getTokenFromHeader(String authorizationHeader) {
        try {
            String[] bearerParts = authorizationHeader.split(" ");
            if (bearerParts.length == 2 && "Bearer".equalsIgnoreCase(bearerParts[0])) {
                return Optional.of(UUID.fromString(bearerParts[1]));
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            return Optional.empty();
        }
    }

    /**
     * @param authorizationHeader http authorization header
     * @return user dto from http authorization header
     */
    private Optional<User> getUserFromHeader(String authorizationHeader) {
        try {
            String[] authParts = authorizationHeader.split(" ");
            if ("Basic".equalsIgnoreCase(authParts[0])) {
                String decodedCredentials = new String(Base64.getDecoder().decode(authParts[1]));
                String[] credentials = decodedCredentials.split(":");
                return Optional.of(new User().setName(credentials[0]).setPassword(credentials[1]));
            } else {
                return Optional.empty();
            }
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    /**
     * @param user to control
     * @return true is the user exists and the password is good
     */
    private boolean isUserValid(User user) {
        return userRepository.findById(user.getName())
                .map(userEntity -> userEntity.getPassword().equals(DigestUtils.sha256Hex(user.getPassword())))
                .orElse(false);
    }

    /**
     * @param user from whom token is generated
     * @return generated token
     */
    private String generateToken(User user) {
        TokenEntity tokenEntity = new TokenEntity()
                .setUser(new UserEntity().setName(user.getName()))
                .setTimestamp(LocalDateTime.now());
        tokenRepository.save(tokenEntity);
        return tokenEntity.getToken().toString();
    }

}

# Vortivox API

API for Vortivox, a very simple API for a very simple social network, not for production, just for fun :)
Springboot is used.

## Users

Users are just a simple combination of a username and a password.

### Tokens

Tokens are UUID generated at each connection and deleted at disconnection or when it expires.
Each request except for creating a user and to connect need the user's token.
With the token, the username is found in the token database.
Tokens are associated with the user and has a timestamp.

## Posts

Posts can be voted up or down, and it affects its chances to be randomly picked.

### Votes

Votes can be up or down and are associated with the post and to the user who has voted.